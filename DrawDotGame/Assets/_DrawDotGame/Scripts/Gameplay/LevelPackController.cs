﻿using UnityEngine;
using System.Collections;

public class LevelPackController : MonoBehaviour
{

    public bool isLocked;

    public void HandleLevelPackButton()
    {
        if (!isLocked)
        {
            LevelSelectionUIManager levelSLUI = FindObjectOfType<LevelSelectionUIManager>();
            levelSLUI.HandleLevelPackButton();
            SgLib.SoundManager.Instance.PlaySound(SgLib.SoundManager.Instance.button);
            int level = int.Parse(GetComponentInChildren<UnityEngine.UI.Text>().text.Split('/')[0]);
            LevelScroller.levelSnapped = level;
            FindObjectOfType<LevelScroller>().ScrollToSelectedPack();
        }  
    }
}
