﻿using UnityEngine;

public class ObstacleId : MonoBehaviour {
    [Header("Do not modify these value")]
    public int id;
    public bool isDeadObstacle;
}
