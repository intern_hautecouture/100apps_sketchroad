﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using SgLib;
using NendUnityPlugin.Common;
using NendUnityPlugin.AD.Video;

public enum GameState
{
    Prepare,
    Playing,
    Paused,
    PreGameOver,
    GameOver
}

public enum Mode
{
    LevelEditorMode,
    GameplayMode
}

public class GameManager : MonoBehaviour
{
    public static event System.Action<GameState, GameState> GameStateChanged = delegate { };
    public static event System.Action<bool, bool> GameEnded = delegate { };

    public GameState GameState
    {
        get
        {
            return _gameState;
        }
        private set
        {
            if (value != _gameState)
            {
                GameState oldState = _gameState;
                _gameState = value;

                GameStateChanged(_gameState, oldState);
            }
        }
    }

    private GameState _gameState = GameState.Prepare;

    public static int GameCount
    {
        get { return _gameCount; }
        private set { _gameCount = value; }
    }

    private static int _gameCount = 0;

    // The selected level
    public static int levelLoaded;

    public ObstacleManager obstacleManager;
    public GameplayUIManager gameplayUIManager;
    public GameObject pinkBallPrefab;
    public GameObject blueBallPrefab;
    public GameObject hintPrefab;
    public ParticleSystem explores;
    public ParticleSystem winning;
    public Material line;
    [HideInInspector]
    public bool gameOver;
    [HideInInspector]
    public bool win = false;
    public Mode mode;
    [HideInInspector]
    public string failedScreenshotName = "failedLevel.png";

    [Header("Gameplay Config")]
    [Tooltip("The color of the drawn lines")]
    public Color lineColor;
    [Tooltip("How many hearts spent to view 1 hint")]
    public int heartsPerHint = 1;
    [Tooltip("How many hearts should be awarded when the player solves a level for the first time, put 0 to disable this feature")]
    public int heartsPerWin = 0;

    [Header("Hint Ad Config")]
    [SerializeField]
    NendUtils.Account account;

    private LevelManager levelManager;
    private List<GameObject> listLine = new List<GameObject>();
    private List<Vector2> listPoint = new List<Vector2>();
    private GameObject currentLine;
    private GameObject currentColliderObject;
    private GameObject hintTemp;
    private BoxCollider2D currentBoxCollider2D;
    private LineRenderer currentLineRenderer;
    private Rigidbody2D pinkBallRigid;
    private Rigidbody2D blueBallRigid;
    private bool stopHolding;
    private bool allowDrawing = true;

    private List<Rigidbody2D> listObstacleNonKinematic = new List<Rigidbody2D>();
    private GameObject[] obstacles;

    private NendAdRewardedVideo rewardHintAd;
    private bool watchedAd = false;
    private const string HINT_ALERT_PPKEY = "SGLIB_HINT_ALERT";


    void Start()
    {
        if (mode == Mode.GameplayMode) //On gameplay mode
        {
            string spotId = "";
            string apiKey = "";

#if UNITY_IOS
            spotId = account.iOS.spotID.ToString();
            apiKey = account.iOS.apiKey;
#elif UNITY_ANDROID
            spotId = account.android.spotID.ToString();
            apiKey = account.android.apiKey;
#endif

            rewardHintAd = NendAdRewardedVideo.NewVideoAd(spotId, apiKey);

            rewardHintAd.AdCompleted += (instance) => {
                //ShowHint();
                watchedAd = true;
            };

            rewardHintAd.Load();

            GameState = GameState.Prepare;
            string path = LevelScroller.JSON_PATH;
            TextAsset textAsset = Resources.Load<TextAsset>(path);
            string[] data = textAsset.ToString().Split(';');
            foreach (string o in data)
            {
                LevelData levelData = JsonUtility.FromJson<LevelData>(o);
                if (levelData.levelNumber == levelLoaded)
                {
                    CreateLevel(levelData);
                    GameState = GameState.Playing;
                    break;
                }
            }

            
        }
        else //On level editor mode
        {
            BallController[] ballsController = FindObjectsOfType<BallController>();
            foreach (BallController o in ballsController)
            {
                if (o.name.Split('(')[0].Equals("PinkBall"))
                    pinkBallRigid = o.gameObject.GetComponent<Rigidbody2D>();
                else
                    blueBallRigid = o.gameObject.GetComponent<Rigidbody2D>();
            }
            pinkBallRigid.isKinematic = true;
            blueBallRigid.isKinematic = true;
            levelManager = FindObjectOfType<LevelManager>();
        }

        obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject o in obstacles)
        {
            Rigidbody2D rigid = o.GetComponent<Rigidbody2D>();
            if (rigid != null && !rigid.isKinematic)
            {
                listObstacleNonKinematic.Add(rigid);
                rigid.isKinematic = true;
            }
        }

    }

    public void Win()
    {
        win = true;
        StopAllPhysics();
        SoundManager.Instance.PlaySound(SoundManager.Instance.win);

        if (mode == Mode.GameplayMode)
        {
            bool firstWin = !LevelManager.IsLevelSolved(levelLoaded);   // solved for the first time

            if (firstWin)
            {
                LevelManager.MarkLevelAsSolved(levelLoaded);
            }

            StartCoroutine(CRTakeScreenshot());
            GameEnded(win, firstWin);    // fire event
        }
    }

    public void GameOver()
    {
        win = false;
        gameOver = true;
        GameState = GameState.GameOver;
        SoundManager.Instance.PlaySound(SoundManager.Instance.gameOver);

        if (mode == Mode.GameplayMode)
        {
            StartCoroutine(CRTakeScreenshot());
            GameEnded(win, false);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    //Create level base on level data
    void CreateLevel(LevelData levelData)
    {
        GameObject pinkBall = (GameObject)Instantiate(pinkBallPrefab, levelData.pinkBallPosition, Quaternion.identity);
        GameObject blueBall = (GameObject)Instantiate(blueBallPrefab, levelData.blueBallPosition, Quaternion.identity);

        pinkBallRigid = pinkBall.GetComponent<Rigidbody2D>();
        pinkBallRigid.isKinematic = true;

        blueBallRigid = blueBall.GetComponent<Rigidbody2D>();
        blueBallRigid.isKinematic = true;

        foreach (ObstacleData o in levelData.listObstacleData)
        {
            foreach (GameObject a in obstacleManager.obstacles)
            {
                if (a.name.Equals(o.id))
                {
                    GameObject obstacle = Instantiate(a, o.position, o.rotation) as GameObject;
                    obstacle.transform.localScale = o.scale;

                    if (a.name.Contains("Conveyor"))
                    {
                        var conveyorController = obstacle.GetComponent<ConveyorController>();
                        conveyorController.rotateDirection = o.rotateDirection;
                        conveyorController.rotateSpeed = o.rotatingSpeed;
                    }

                    break;
                }
            }
        }
    }

    void Update()
    {
        if(watchedAd)
        {
            watchedAd = false;
            
            ShowHint();
            var gameplayUIManager = GameObject.Find("GamePlayUI").GetComponent<GameplayUIManager>();
            gameplayUIManager.HideHintAlert();
        }

        if (!gameOver && !win)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Get the button on click
                GameObject thisButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject;

                if (thisButton != null)//Is click on button
                {
                    allowDrawing = false;
                }
                else //Not click on button
                {
                    allowDrawing = true;

                    //Is on gameplay mode
                    if (mode == Mode.GameplayMode)
                    {
                        if (gameplayUIManager.btnHint.activeSelf)
                            gameplayUIManager.btnHint.SetActive(false);
                        if (hintTemp != null)
                            Destroy(hintTemp);
                    }
                    stopHolding = false;
                    listPoint.Clear();
                    CreateLine(Input.mousePosition);
                }
            }
            else if (Input.GetMouseButton(0) && !stopHolding && allowDrawing)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                if (!listPoint.Contains(mousePos))
                {
                    //Add mouse pos, set vertex and position for line renderer
                    listPoint.Add(mousePos);
#if UNITY_5_5_OR_NEWER
                    currentLineRenderer.positionCount = listPoint.Count;
#else
                    currentLineRenderer.SetVertexCount(listPoint.Count);
#endif
                    currentLineRenderer.SetPosition(listPoint.Count - 1, listPoint[listPoint.Count - 1]);

                    //Create collider
                    if (listPoint.Count >= 2)
                    {
                        Vector2 point_1 = listPoint[listPoint.Count - 2];
                        Vector2 point_2 = listPoint[listPoint.Count - 1];

                        currentColliderObject = new GameObject("Collider");
                        currentColliderObject.transform.position = (point_1 + point_2) / 2;
                        currentColliderObject.transform.right = (point_2 - point_1).normalized;
                        currentColliderObject.transform.SetParent(currentLine.transform);

                        currentBoxCollider2D = currentColliderObject.AddComponent<BoxCollider2D>();
                        currentBoxCollider2D.size = new Vector3((point_2 - point_1).magnitude, 0.1f, 0.1f);
                        currentBoxCollider2D.enabled = false;

                        Vector2 rayDirection = currentColliderObject.transform.TransformDirection(Vector2.right);

                        Vector2 pointDir = currentColliderObject.transform.TransformDirection(Vector2.up);

                        Vector2 rayPoint_1 = (Vector2)currentColliderObject.transform.position + (-rayDirection) * (currentBoxCollider2D.size.x);

                        Vector2 rayPoint_2 = ((Vector2)currentColliderObject.transform.position + pointDir * (currentBoxCollider2D.size.y / 2f))
                                             + ((-rayDirection) * (currentBoxCollider2D.size.x));

                        Vector2 rayPoint_3 = ((Vector2)currentColliderObject.transform.position + (-pointDir) * (currentBoxCollider2D.size.y / 2f))
                                             + ((-rayDirection) * (currentBoxCollider2D.size.x));

                        float rayLength = ((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) - rayPoint_1).magnitude;

                        RaycastHit2D hit_1 = Physics2D.Raycast(rayPoint_1, rayDirection, rayLength);
                        RaycastHit2D hit_2 = Physics2D.Raycast(rayPoint_2, rayDirection, rayLength);
                        RaycastHit2D hit_3 = Physics2D.Raycast(rayPoint_3, rayDirection, rayLength);

                        if (hit_1.collider != null || hit_2.collider != null || hit_3.collider != null)
                        {
                            GameObject hit = (hit_1.collider != null) ? (hit_1.collider.gameObject) :
                                            ((hit_2.collider != null) ? (hit_2.collider.gameObject) : (hit_3.collider.gameObject));
                            if (currentColliderObject.transform.parent != hit.transform.parent)
                            {
                                Destroy(currentBoxCollider2D.gameObject);

                                if (pinkBallRigid.isKinematic)
                                    pinkBallRigid.isKinematic = false;
                                if (blueBallRigid.isKinematic)
                                    blueBallRigid.isKinematic = false;

                                for (int i = 0; i < currentLine.transform.childCount; i++)
                                {
                                    currentLine.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
                                }

                                if (mode == Mode.LevelEditorMode)
                                {
                                    levelManager.listLineRendererPos = listPoint;
                                }

                                listLine.Add(currentLine);
                                currentLine.AddComponent<Rigidbody2D>().useAutoMass = true;
                                foreach (Rigidbody2D rigid in listObstacleNonKinematic)
                                {
                                    rigid.isKinematic = false;
                                }
                                stopHolding = true;
                            }
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0) && !stopHolding && allowDrawing)
            {
                if (pinkBallRigid.isKinematic)
                    pinkBallRigid.isKinematic = false;
                if (blueBallRigid.isKinematic)
                    blueBallRigid.isKinematic = false;

                if (mode == Mode.LevelEditorMode)
                {
                    levelManager.listLineRendererPos = listPoint;
                }

                if (currentLine.transform.childCount > 0)
                {
                    for (int i = 0; i < currentLine.transform.childCount; i++)
                    {
                        currentLine.transform.GetChild(i).GetComponent<BoxCollider2D>().enabled = true;
                    }
                    listLine.Add(currentLine);
                    currentLine.AddComponent<Rigidbody2D>().useAutoMass = true;
                }
                else
                {
                    Destroy(currentLine);
                }

                foreach (Rigidbody2D rigid in listObstacleNonKinematic)
                {
                    rigid.isKinematic = false;
                }
            }
        }
    }


    void CreateLine(Vector2 mousePosition)
    {
        currentLine = new GameObject("Line");
        currentLineRenderer = currentLine.AddComponent<LineRenderer>();
        currentLineRenderer.material = line;
        currentLineRenderer.material.EnableKeyword("_EMISSION");
        currentLineRenderer.material.SetColor("_EmissionColor", lineColor);
#if UNITY_5_5_OR_NEWER
        currentLineRenderer.positionCount = 0;
        currentLineRenderer.startWidth = 0.1f;
        currentLineRenderer.endWidth = 0.1f;
        currentLineRenderer.startColor = lineColor;
        currentLineRenderer.endColor = lineColor;
#else

        currentLineRenderer.SetVertexCount(0);
        currentLineRenderer.SetWidth(0.1f, 0.1f);
        currentLineRenderer.SetColors(lineColor, lineColor);
#endif
        currentLineRenderer.useWorldSpace = false;
    }

    public void StopAllPhysics()
    {
        pinkBallRigid.isKinematic = true;
        blueBallRigid.isKinematic = true;
#if UNITY_5_5_OR_NEWER
        pinkBallRigid.simulated = false;
        blueBallRigid.simulated = false;
#endif
        for (int i = 0; i < listLine.Count; i++)
        {
            Rigidbody2D rigid = listLine[i].GetComponent<Rigidbody2D>();
            rigid.isKinematic = true;
#if UNITY_5_5_OR_NEWER
            rigid.simulated = false;
#endif
        }
    }

    // Capture a screenshot when game ends
    private IEnumerator CRTakeScreenshot()
    {
        // Capture the screenshot that is 2x bigger than the displayed one for crisper image on retina displays
        int height = LevelManager.ssHeight * 2;
        int width = (int)(height * Screen.width / Screen.height);
        RenderTexture rt = new RenderTexture(width, height, LevelManager.bitType, RenderTextureFormat.ARGB32);
        yield return new WaitForEndOfFrame();
        Camera.main.targetTexture = rt;
        Camera.main.Render();
        yield return null;
        Camera.main.targetTexture = null;
        yield return null;

        RenderTexture.active = rt;
        Texture2D tx = new Texture2D(width, height, TextureFormat.ARGB32, false);
        tx.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tx.Apply();
        RenderTexture.active = null;
        Destroy(rt);

        byte[] bytes = tx.EncodeToPNG();

        // Store the screenshot with the level number and overwrite the old level screenshot if win, otherwise store a failed screenshot
        string filename = win ? levelLoaded.ToString() + ".png" : failedScreenshotName;
        string imgPath = Path.Combine(Application.persistentDataPath, filename);
        File.WriteAllBytes(imgPath, bytes);
    }

    public bool ShowHint()
    {
        // Only show hint if there's enough hearts
        // if (CoinManager.Instance.Coins >= heartsPerHint)
        //{
        string path = LevelScroller.JSON_PATH;
        TextAsset textAsset = Resources.Load<TextAsset>(path);
        string[] data = textAsset.ToString().Split(';');
        foreach (string o in data)
        {
            LevelData levelData = JsonUtility.FromJson<LevelData>(o);
            if (levelData.levelNumber == levelLoaded)
            {
                hintTemp = Instantiate(hintPrefab, levelData.hintData.position, levelData.hintData.rotation) as GameObject;
                hintTemp.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Hints/hint" + levelLoaded.ToString());
                hintTemp.gameObject.transform.localScale = levelData.hintData.scale;

                // Remove hearts
                CoinManager.Instance.RemoveCoins(heartsPerHint);
                PlayerPrefs.SetInt(HINT_ALERT_PPKEY, 1);

                return true;
            }
        }
        //}

        return false;
    }

    public void ShowHintAd()
    {
        if (rewardHintAd.IsLoaded())
        {
            Debug.Log("ad loaded");
            rewardHintAd.Show();
        }
    }
}
