﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;


public class LevelManager : MonoBehaviour
{

    public static int ssWidth = 230;
    public static int ssHeight = 130;
    public static int bitType = 16;

    public List<Vector2> listLineRendererPos;

    public static string JsonPath()
    {
        string path = "Assets/_DrawDotGame/Resources/Json/LevelsData.json";
        return path;
    }

    public static string ScreenshotPath(int levelNumber)
    {
        string path = "Assets/_DrawDotGame/Resources/Screenshots/" + levelNumber.ToString() + ".png";
        return path;
    }

    public void ClearScene()
    {
        GameObject[] allObstacle = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach (GameObject o in allObstacle)
        {
            DestroyImmediate(o);
        }

        BallController[] ballControllers = FindObjectsOfType<BallController>();
        foreach (BallController o in ballControllers)
        {
            DestroyImmediate(o.gameObject);
        }

        DestroyImmediate(GameObject.FindGameObjectWithTag("Hint"));
    }

    public void LoadLevel(int levelNumber)
    {
        ClearScene();
        StreamReader reader = new StreamReader(JsonPath());
        string[] data = reader.ReadToEnd().Split(';');

        for (int i = 0; i < data.Length; i++)
        {
            LevelData levelData = JsonUtility.FromJson<LevelData>(data[i]);
            if (levelData.levelNumber == levelNumber)
            {
                //Create balls
                GameManager gameManager = FindObjectOfType<GameManager>();
                Instantiate(gameManager.pinkBallPrefab, levelData.pinkBallPosition, Quaternion.identity);
                Instantiate(gameManager.blueBallPrefab, levelData.blueBallPosition, Quaternion.identity);


                GameObject hint = (GameObject)Instantiate(gameManager.hintPrefab, levelData.hintData.position, levelData.hintData.rotation);
                hint.transform.localScale = levelData.hintData.scale;
                hint.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Hints/hint" + levelData.levelNumber.ToString());

                foreach (ObstacleData o in levelData.listObstacleData)
                {
                    foreach (GameObject a in gameManager.obstacleManager.obstacles)
                    {
                        if (o.id.Trim().Equals(a.name.Trim()))
                        {
                            //Create obstacles 
                            GameObject obstacle = Instantiate(a, o.position, o.rotation) as GameObject;
                            obstacle.transform.localScale = o.scale;
                            ConveyorController carouselController = obstacle.GetComponent<ConveyorController>();
                            if (carouselController != null)
                            {
                                carouselController.rotateDirection = o.rotateDirection;
                                carouselController.rotateSpeed = o.rotatingSpeed;
                            }
                            break;
                        }
                    }
                }

                break;
            }
        }
        reader.Close();
    }

    public void OverwriteLevel(int levelNumber)
    {
        string[] data = null;
        StreamReader reader = new StreamReader(JsonPath());
        data = reader.ReadToEnd().Split(';');
        for (int i = 0; i < data.Length; i++)
        {
            LevelData levelData = JsonUtility.FromJson<LevelData>(data[i]);
            if (levelData.levelNumber == levelNumber)
            {
                string dataToOverride = GetLevelData(levelNumber);
                data[i] = "\n" + dataToOverride;
                data[i].Trim();
                break;
            }
        }
        reader.Close();
        OverrideJsonFile(data);
        TakeScreenshot(levelNumber);
    }

    void OverrideJsonFile(string[] data)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.Length; i++)
        {
            sb.Append((i == data.Length - 1) ? (data[i]) : (data[i] + ";"));
        }

        using (StreamWriter writer = new StreamWriter(JsonPath(), false))
        {
            writer.Write(sb.ToString());
            writer.Close();
        }
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif      
    }

    public void TakeScreenshot(int levelNumber)
    {
        StartCoroutine(SnapShot(ssWidth, ssHeight, levelNumber));
    }

    IEnumerator SnapShot(int width, int height, int levelNumber)
    {
        // Hide the hint
        GameObject hint = GameObject.FindGameObjectWithTag("Hint");
        hint.SetActive(false);

        // Capture screenshot using render texture
        RenderTexture rt = new RenderTexture(width, height, bitType, RenderTextureFormat.ARGB32);
        yield return new WaitForEndOfFrame();
        Camera.main.targetTexture = rt;
        Camera.main.Render();
        Camera.main.targetTexture = null;

        RenderTexture.active = rt;
        Texture2D tx = new Texture2D(width, height, TextureFormat.ARGB32, false);
        tx.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tx.Apply();
        RenderTexture.active = null;
        DestroyImmediate(rt);

        byte[] bytes = tx.EncodeToPNG();
        File.WriteAllBytes(ScreenshotPath(levelNumber), bytes);
        hint.SetActive(true);
    }

    public static int GetTotalLevelNumber()
    {
        if (File.Exists(JsonPath()))
        {
            StreamReader reader = new StreamReader(JsonPath());
            string[] data = reader.ReadToEnd().Split(';');
            reader.Close();
            return data.Length;
        }
        else
        {
            return 0;
        }
    }

    public void SaveLevel(int currentLevel)
    {
        BallController[] ballsController = FindObjectsOfType<BallController>();

        if (ballsController == null || ballsController.Length < 2)
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.DisplayDialog("Level Not Saved", "There must be two balls in a level!", "OK");
#endif
            return;
        }
        else
        {
            if (currentLevel < 0)
            {
#if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayDialog("Error", "The level number must be a positive number!", "OK");
#endif
                return;
            }
            else
            {
                string data = GetLevelData(currentLevel);
                AddDataForJson(data);
                TakeScreenshot(currentLevel);

#if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayDialog("Level Saved", "Level " + currentLevel.ToString() + " was saved!", "OK");
#endif
            }
        }
    }

    string GetLevelData(int currentLevel)
    {
        BallController[] ballsController = FindObjectsOfType<BallController>();
        GameObject blueBall = null;
        GameObject pinkBall = null;
        foreach (BallController o in ballsController)
        {
            if (o.gameObject.name.Split('(')[0].Trim().Equals("PinkBall"))
                pinkBall = o.gameObject;
            else
                blueBall = o.gameObject;
        }

        GameObject[] allObstacle = GameObject.FindGameObjectsWithTag("Obstacle");
        GameObject hint = GameObject.FindGameObjectWithTag("Hint");

        List<ObstacleData> listObstacleData = new List<ObstacleData>();
        for (int i = 0; i < allObstacle.Length; i++)
        {
            ObstacleData obstacleData = new ObstacleData();
            string id = allObstacle[i].name.Split('(')[0].Trim();
            obstacleData.id = id;
            obstacleData.position = allObstacle[i].gameObject.transform.position;
            obstacleData.rotation = allObstacle[i].gameObject.transform.rotation;
            obstacleData.scale = allObstacle[i].gameObject.transform.localScale;

            ConveyorController carouselController = allObstacle[i].GetComponent<ConveyorController>();
            if (carouselController != null)
            {
                obstacleData.rotateDirection = carouselController.rotateDirection;
                obstacleData.rotatingSpeed = carouselController.rotateSpeed;
            }

            listObstacleData.Add(obstacleData);
        }
        HintData hintData = new HintData();
        hintData.position = hint.transform.position;
        hintData.rotation = hint.transform.rotation;
        hintData.scale = hint.transform.localScale;

        LevelData levelData = new LevelData();
        levelData.levelNumber = currentLevel;
        levelData.blueBallPosition = blueBall.transform.position;
        levelData.pinkBallPosition = pinkBall.transform.position;
        levelData.listObstacleData = listObstacleData;
        levelData.hintData = hintData;

        string data = JsonUtility.ToJson(levelData);
        return data;
    }

    void AddDataForJson(string data)
    {
        data.Trim();
        if (File.Exists(JsonPath())) //File already exists 
        {
            using (StreamWriter writer = new StreamWriter(JsonPath(), true))
            {
                string currentData = ";\n" + data;
                writer.Write(currentData);
                writer.Close();
            }
        }
        else //File not exists -> create new json file
        {
            using (StreamWriter writer = new StreamWriter(JsonPath(), true))
            {
                writer.Write(data);
                writer.Close();
            }
        }

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif      
    }

    //Check the given level is solved or not
    public static bool IsLevelSolved(int levelNumber)
    {
        string[] data = PlayerPrefs.GetString("LevelsSolved").Split(';');
        foreach (string o in data)
        {
            if (o.Equals(levelNumber.ToString()))
                return true;
        }
        return false;
    }

    // Store the solved level number to PlayerPrefs to remember that it's solved
    public static void MarkLevelAsSolved(int level)
    {
        string levelsSolvedData = PlayerPrefs.GetString("LevelsSolved");
        if (levelsSolvedData.Length == 0)
        {
            levelsSolvedData = level.ToString();
            PlayerPrefs.SetString("LevelsSolved", levelsSolvedData);
        }
        else
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder(levelsSolvedData);

            string[] data = sb.ToString().Split(';');

            foreach (string o in data)
            {
                if (int.Parse(o) == level)
                {
                    return;
                }
            }
            sb.Append(";" + level.ToString());
            PlayerPrefs.SetString("LevelsSolved", sb.ToString());
        }
    }
}
